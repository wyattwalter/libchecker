from flask import Flask, request, render_template, url_for, redirect
from lib.hclib import HCLib
from lib.wishlist import WishList
import re

app = Flask(__name__)
app.config.from_object(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        form = getattr(request, 'form', None)
        if form:
            error = None
            url = form['wishlistUrl']
            lib = HCLib()
            try:
                wishlist = WishList(url)
            except:
                return render_template('index.html', error='Invalid wishlist URL')
            items = wishlist.get_items()
            for item in items:
                item['available'] = False
                results = lib.search(search_string(item['title']))
                #results = None
                if results:
                    for result in results:
                        # hclib only returns one author, check if it's in the list of authors from Amazon
                        if result['author']:
                            if result['author'].lower() in [x.lower() for x in item['authors']]:
                                # pretty confident we have a match at this point
                                item['available'] = True
                                item['link'] = result['link']
                        # TODO: some better matching using fuzzy string match
                item['authors'] = ', '.join(item['authors'])
            return render_template('index.html', wishlist=items)
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

def search_string(string):
    string = string.split(':')[0]
    pattern = re.compile(r'(\,[\w\d\s]*edition)', re.IGNORECASE)
    return pattern.sub('', string)


if __name__ == '__main__':
    app.debug = True
    app.run()
