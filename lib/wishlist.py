from bs4 import BeautifulSoup
import urllib2
import re


class WishList:

    print_base = 'http://www.amazon.com/gp/registry/wishlist/ref=cm_wl_act_print_o?ie=UTF8&disableNav=1&filter=all&id={0}&items-per-page=200&layout=standard-print&sort=rank&visitor-view='

    def __init__(self, url):
        re_id = re.compile(r'https?://(?:(?:www|smile)\.amazon.com/gp/(?:registry/wishlist/|aw/ls/.*lid=)|amzn.com/w/)([\d\w]+)/?')
        match = re_id.match(url)
        if not match:
            raise ValueError('Invalid wishlist URL')
        self.id = match.groups()[0]

    def print_url(self):
        return self.print_base.format(self.id)

    def get_items(self):
        re_author = re.compile(r'^ by (.*) \([\w ]+\)')
        # cleanup zero width space in response from Amazon
        re_zws = re.compile(ur'\u200b', re.UNICODE)

        page = urllib2.urlopen(self.print_url())
        soup = BeautifulSoup(page.read(), 'html.parser')

        books = []

        for book in soup.find_all('td', class_='g-title'):
            authors = []
            for span in book.find_all('span'):
                m = re_author.match(span.string)
                if m:
                    authors = m.groups()[0].split(', ')
            title = re_zws.sub('', book.find('h5').string)
            try:
                parent = book.parent
                img = parent.find('td', class_='g-image').img['src']
            except:
                # img retrieval is likely fragile and not necessary
                pass
            books.append({'title': title,
                          'authors': authors,
                          'img': img})
        return books
