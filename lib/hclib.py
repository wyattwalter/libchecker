from bs4 import BeautifulSoup
import urllib
import urllib2
import re

class HCLib:

    base_url = 'https://apps.hclib.org'

    def search(self, title):
        search_url = self.base_url + '/catalog/results.cfm?all='
        q = urllib.quote(title)
        url = search_url + q
        page = self.req(url)
        return self.parse(page)

    def req(self, url):
        page = urllib2.urlopen(url).read()
        return page

    def parse(self, page):
        results = []
        soup = BeautifulSoup(page, 'html.parser')
        articles = soup.find_all('article')
        if not articles:
            return None
        for article in articles:
            title = article.find('a', class_='record-title').string
            link = self.base_url + article.find('a', class_='record-title')['href']
            # the search listings seem to always only list one author in the form Last, First
            # do the transpose of name here
            author = article.find('a', class_='record-author')
            if author:
                author_string = author.string
                author_pieces = author_string.split(',')
                if len(author_pieces) > 1:
                    author = author_pieces[1].strip() + u' ' + author_pieces[0].strip()
                else:
                    author = author_string
            results.append({'title': title, 'author': author, 'link': link})
        return results
